package com.epam.nickscout;


import com.epam.nickscout.model.droids.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;

public class Main {

    public static Logger log = LogManager.getLogger();

    public static void main(String[] args) {
        Ship ship = new Ship();
        ship.randomFillIn();
        ship.save();
        log.info("ship before alteration: " + ship.toString());
        ship.setDroids(new HashSet<>());
        log.info("altered ship: " + ship.toString());
        ship.load();
        log.info("backed up ship: " + ship.toString());
    }
}
