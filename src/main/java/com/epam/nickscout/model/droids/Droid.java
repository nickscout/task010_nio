package com.epam.nickscout.model.droids;

import lombok.Data;

import java.io.Serializable;

@Data
public class Droid implements Serializable {
    private transient LaserGun laserGun;
    private int healthPoints;
    private int shieldPoints;
}
