package com.epam.nickscout.model.droids;

import lombok.Data;

import java.io.*;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

@Data
public class Ship implements Serializable {
    private Set<Droid> droids;

    public Ship() {
        droids = new HashSet<>();
    }

    public void randomFillIn() {
        Random random = new Random();
        droids = new HashSet<>();
        int amount = random.ints(5, 15).findFirst().getAsInt();
        for (int i = 0; i < amount; i++) {
            Droid droid = new Droid();
            droid.setLaserGun(new LaserGun());
            droid.setHealthPoints(random.nextInt(100));
            droid.setShieldPoints(random.nextInt(50));
            droids.add(droid);
        }
    }

    public void save() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("droids.dat").getFile());
        try (
                FileOutputStream fos = new FileOutputStream(file);
                ObjectOutputStream out = new ObjectOutputStream(fos);
                ) {
            out.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void load() {
        ClassLoader classLoader = getClass().getClassLoader();
        try(
                InputStream is = classLoader.getResourceAsStream("droids.dat");
                ObjectInputStream in = new ObjectInputStream(is)
                ) {
            Ship ship = (Ship) in.readObject();
            this.droids = ship.droids;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
